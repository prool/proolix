Proolix-l operation system
--------------------------

diskette.img - Proolix-l bootable diskette (current)

proolix.iso - Proolix-l bootable CDROM (old)

-----

how to easy make proolix-l bootable flash USB stick

use 'unetbootin' utility and proolix diskette image

-----

How to make bootable CDROM:

genisoimage -o iso.iso -b diskette.img .

(point '.' in end of line NEED!
it's 'unix current path')

-----

how to make empty diskette image with FAT-12 filesystem (not need for current version of Proolix)

dd if=/dev/zero of=empty-diskette2.img bs=512 count=2880
mkfs.vfat -F 12 empty-diskette2.img

-----

how to easy make Proolix-l bootable diskette image (for example for VirtualBox or other virtual machines)

1. make empty diskette image
dd if=/dev/zero of=diskette.img bs=512 count=2880

2. install boot sector to this file (in directory src-bootsector)
	make bootfdd
	dd if=bootfdd of=../../diskette.img conv=notrunc

3. install Proolix-l kernel to diskette (in directory src/stage2)
	make
	dd if=ct of=../../diskette.img seek=1 conv=notrunc

4. formatting proolix filesystem:
	boot from diskette
	format
	0
	y
	
	