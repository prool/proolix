; fasm util for proolix
; compiling using fasm
;
; this is msdos-like .com file
; execute in Proolix with command 'rundos'
;
; /Prool
;
	org	100H

	push	ES	; save ES

	push	DS
	pop	ES	; ES:=DS

;	mov	ah,0eh
;	mov	al,'%'
;	int	10h

	mov	ah,9
	mov	dx,string
	int	21h

	mov	ah,2ch
	int	21h
	mov	ax,cx
	call	ohw

l_exit:
	pop	ES	; restore ES

	int	20h

include		"ohw.asm"
include		"ohb.asm"

string:		db	"This is fasm .com program"
		db	13,10,"$"

buffer:
