; library for nasm programs for OS Proolix

; /prool, december 2022, Kharkiv, Ukraine, war
; www.prool.kharkov.org

print_char:	; al - char
	push	ax
	mov	ah,2
	int	0x91
	pop	ax
	ret

print_string_dos:	; dx - addr of string, terminated of '$' (MSDOS style)
	push 	ax
	mov	ah,9
	int	0x21
	pop	ax
	ret

getchar:

	mov	ah,1
	int	0x91 ; read char to al
	ret

getstr:	; get string from input to [ax]
	push	ax
	push	di

	mov	di,ax	
l_1:
	call	getchar
	call	print_char
	cmp	al,10
	je	l_2
	cmp	al,13
	je	l_2
	mov	[di],al
	inc	di
	jmp	l_1
l_2:
	mov	byte [di],0

	pop	di
	pop	ax
	ret
	
print_string: ; print string [dx], terminated by zero or '$'
	push	ax
	push	di
	
	mov	di,dx
.l_loop:
	mov	al,byte [di]
	cmp	al,0
	je	.l_ret
	cmp	al,'$'
	je	.l_ret
	call	print_char
	inc	di
	jmp	.l_loop
.l_ret:
	pop	di
	pop	ax
	ret

crlf:
	push	dx
	mov	dx,s_crlf
	call	print_string
	pop	dx
	ret

space:	;
	push	ax
	mov	al,' '
	mov	ah,2
	int	0x91
	pop	ax
	ret

; ohb

ohb:

; Procedure output hex byte Ver 0.1.1 6 Dec 93 via BIOS
; Input: AL - byte
; All regs. reserved ;)

; Not worked in graph mode. bl - bg color !!!

        push    ax
        push    cx
        push    dx

        mov     dl,al
        mov     cl,4
        shr     al,cl
        call    ohb1

        mov     al,dl
        and     al,0fh
        call    ohb1

        pop     dx
        pop     cx
        pop     ax

        ret

ohb1:    ; Regs not saved !!!
        push    ax

        cmp     al,9
        ja      @@_1    ; al > 9
        ; al <= 9
        add     al,'0'
        jmp     @@_out

@@_1:   add     al,'A'-10
@@_out: mov     ah,0eh
        int     10h

        pop     ax

        ret

ohd:	; write hex number from eax
	push	eax

	push	eax
	shr	eax,16
	call	ohw
	pop	eax
	call	ohw

	pop	eax
	ret

; ohw

ohw:

; print hex word. Parameter: ax - word
; all regs saved

        push    ax	; save al ;)
        mov     al,ah
        call    ohb
        pop     ax      ; restore al
        call    ohb
        ret

s_crlf:	db	13,10,'$'

htoax: ; convert hex string to number to register ax
; input: string address in bx
; output: number in ax
; regs saved

	push	bx
	push	cx

;; int htoi(const char  *s)
;; {
;; int i; char c;
;; i=0;

	xor	ax,ax

;; while (*s)

.l_loop:

	mov	cl,[bx]
	or	cl,cl
	jz	.l_break
	
;;   {
;;   i<<=4;

	shl	ax,4

;;   if ((c>='0')&&(c<='9')) i+=c-'0';
;;   else if ((c>='A')&&(c<='F')) i+=c+10-'A';
;;   else if ((c>='a')&&(c<='f')) i+=c+10-'a';
	cmp	cl,'0'
	jb	.l_continue	; cl<'0'
	cmp	cl,'9'
	jbe	.l_digit	; cl<='9'
	cmp	cl,'A'
	jb	.l_continue	; cl<'A'
	cmp	cl,'F'
	jbe	.l_AF		; cl<='F'
	cmp	cl,'a'
	jb	.l_continue	; cl<'a'
	cmp	cl,'f'
	jbe	.l_af		; cl<='f'
	jmp	.l_continue	; cl>'f'

.l_digit:
	sub	cl,'0'
	add	al,cl
	jmp	.l_continue

.l_af:
	sub	cl,'a'
	add	cl,0xa
	add	al,cl
	jmp	.l_continue

.l_AF:
	sub	cl,'A'
	add	cl,0xA
	add	al,cl
	;jmp	.l_continue

.l_continue:
	inc	bx
	jmp	.l_loop
;;   }

.l_break:

;; return i;

	pop	cx
	pop	bx

	ret

htoeax: ; convert hex string to number to register eax
; input: string address in bx
; output: number in eax
; regs saved

	push	bx
	push	cx

;; int htoi(const char  *s)
;; {
;; int i; char c;
;; i=0;

	xor	eax,eax

;; while (*s)

.l_loop:

	mov	cl,[bx]
	or	cl,cl
	jz	.l_break
	
;;   {
;;   i<<=4;

	shl	eax,4

;;   if ((c>='0')&&(c<='9')) i+=c-'0';
;;   else if ((c>='A')&&(c<='F')) i+=c+10-'A';
;;   else if ((c>='a')&&(c<='f')) i+=c+10-'a';
	cmp	cl,'0'
	jb	.l_continue	; cl<'0'
	cmp	cl,'9'
	jbe	.l_digit	; cl<='9'
	cmp	cl,'A'
	jb	.l_continue	; cl<'A'
	cmp	cl,'F'
	jbe	.l_AF		; cl<='F'
	cmp	cl,'a'
	jb	.l_continue	; cl<'a'
	cmp	cl,'f'
	jbe	.l_af		; cl<='f'
	jmp	.l_continue	; cl>'f'

.l_digit:
	sub	cl,'0'
	add	al,cl
	jmp	.l_continue

.l_af:
	sub	cl,'a'
	add	cl,0xa
	add	al,cl
	jmp	.l_continue

.l_AF:
	sub	cl,'A'
	add	cl,0xA
	add	al,cl
	;jmp	.l_continue

.l_continue:
	inc	bx
	jmp	.l_loop
;;   }

.l_break:

;; return i;

	pop	cx
	pop	bx

	ret

