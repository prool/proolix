; assembler program for OS Proolix
; for compiling need nasm
;
; interrupt 0x91 - service interrupt in Proolix (similar to MSDOS's int 0x21)
; int 0x90 - quit in Proolix (similar to int 0x20 in MSDOS)
; int 0x21 - also working in Proolix kernel inbound simplest DOS-emulator

; /prool, 2023, Kharkiv, Ukraine, war
; www.prool.kharkov.org

;SECTION .text

    org 0x0 ; begin addr for proolix binaryes

	jmp	l_continue
signature:	dw	0xAAAA
arguments_offset:	dw	0
arguments_seg:		dw	0
	dw	0	; protect minibuffer
stored32:	dd	0

l_continue:

	push CS
	pop	DS

	push CS
	pop ES

	push	cs
	pop	bx

	;cli
	mov	SS,bx
	mov	SP,0xFFFC
	;sti

	;mov al,'!'
	;call	print_char
    
	mov	dx,s_ver
	call	print_string
    
	call	print_help

l_loop:

	mov dx,s_prompt
	call	print_string

	mov	ah,1
	int	0x91 ; read char to al

	mov	dx,s_str2
	call	print_string
	
	call	print_char
	
	call	space
	
	call	ohb

	call	crlf
	
	cmp	al,'m'
	je	l_memmap

	cmp	al,'h'
	je	l_proolix_help
	
	cmp	al,'?'
	je	l_u1_help

	cmp	al,'q'
	je	l_exit
	
	cmp	al,'c'
	je	l_clisti
	
	cmp	al,'1'
	je	l_cli
	
	cmp	al,'2'
	je	l_sti
	
	cmp	al,'3'
	je	l_pgm_3	; cat or more
	
	cmp	al,'4'
	je	l_pgm_4	; create file
	
	cmp	al,'5'
	je	l_pgm_5 ; load file

	cmp	al,'6'
	je	l_pgm_6	; cat loaded file

	cmp	al,'8'
	je	l_pgm_8 ; write loaded file
	
	cmp	al,'0'
	je	l_pgm_0 ; run loaded file

	cmp	al,'M'
	je	l_pgm_M ; more / no more
	
	cmp	al,'W'
	je	l_pgm_W ; write long file
	
	cmp	al,'e'
	je	l_pgm_e ; view exe header

	cmp	al,'t'
	je	l_pgm_t ; test
	
	jmp	l_loop

l_pgm_t:
	
            mov  ax,0x5301           ;Function 5301h: APM þ Connect real-mode interface
            xor  bx,bx             ;Device ID:      0000h (=system BIOS)
            int  0x15                ;Call interrupt: 15h

            mov  ax,0x530e           ;Function 530Eh: APM þ Driver version
            mov  cx,0x0102           ;Driver version: APM v1.2
            int  0x15                ;Call interrupt: 15h

            mov  ax,0x5307           ;Function 5307h: APM þ Set system power state
            mov  bl,01             ;Device ID:      0001h (=All devices)
            mov  cx,0003           ;Power State:    0003h (=Off)
            int  0x15                ;Call interrupt: 15h

	jmp	l_loop

l_pgm_M:
	mov	al,[more]
	or	al,al
	jz	.l_0
;	if more !=0 then more:=0
	xor	al,al
	mov	[more],al
	mov	dx,.s_more_reset
	call	print_string
	jmp	l_loop

.l_0:	; if more==0 then more:=1
	mov	al,1
	mov	[more],al
	mov	dx,.s_more_set
	call	print_string
	jmp	l_loop

.s_more_reset:	db	"More reset to 0",13,10,0
.s_more_set:	db	"More set to 1",13,10,0
more:		db	1
	
l_u1_help:
	call	print_help
	jmp	l_loop

l_proolix_help:
	mov	dx,s_proolix_help
	call	print_string
	jmp	l_loop

l_memmap:
	mov	dx,s_memmap
	call	print_string
	jmp	l_loop
	
l_pgm_4:	; create file
	mov	dx,.s1
	call	print_string
	
	mov	ax,buffer
	call	getstr
	
	call	crlf
	
	mov	dx,buffer
	call	print_string
	
	call	crlf
	
	mov	ah,0x20	; open file
	mov	al,2	; O_CREAT
	mov	bx,buffer	; filename
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err1

.l_read_string:
	mov	dx,.s_prompt
	call	print_string
	
	; read string
	mov	ax,buffer
	call	getstr
	call	crlf
	
	; test string by symbol ')' (end of input)
	cmp	byte [buffer],')'
	je	.l_end_of_input
	
	; write string to file
	mov	si,buffer
.l_loop:
	mov	dl,[si]	; writed char
	cmp	dl,0	; 0 - end of write string
	je	.l_break
	mov	ah,0x22	; write cmd
	mov	al,0	; file descriptor
	int	0x91
	cmp	ax,0xFFFF
	je	.l_write_err
	
	inc	si
	jmp	.l_loop
.l_break:

	; write end of string - code 0x0A (10)
	mov	dl,10
	mov	ah,0x22
	mov	al,0
	int	0x91
	cmp	ax,0xFFFF
	je	.l_write_err

	jmp	.l_read_string
.l_end_of_input:

	; close
	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err2

	jmp	l_loop

.l_err1:
	mov	dx,.s_err1
	call	print_string
	jmp	l_loop

.l_err2:
	mov	dx,.s_err2
	call	print_string
	jmp	l_loop

.l_write_err:
	mov	dx,.s_write_err
	call	print_string
	jmp	l_loop

.s_err1:	db	"Open file error",13,10,0
.s_err2:	db	"Close file error",13,10,0
.s1:		db	"Enter filename for create > ",0
.s_write_err:	db	"Write error",13,10,0
.s_prompt:	db	">$"

l_pgm_3:	; cat or more

	mov	dx,s_filename
	call	print_string

	mov	ax,buffer	
	call	getstr
	
	call	crlf
	;mov	dx,buffer
	;call	print_string
	;call	crlf
	
	mov	ah,0x20	; open file
	mov	al,0	; O_RDONLY
	mov	bx,buffer	; filename
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err

	;mov	dx,ax	; file descriptor
	;call	ohw
	;call	crlf

	xor	bx,bx
	mov	[.counter],bx	; line counter = 0
.l_read:
	mov	ah,0x21	; read 1 byte from file
	mov	al,0	; file descriptor
	int	0x91
	
	cmp	ah,0
	jne	.l_eof
	
	cmp	al,9	; replace TAB to SPACE
	jne	.l_notab
	mov	al,' '
.l_notab:
	call	print_char
	
	cmp	al,10
	jne	.l_next
	mov	al,13
	call	print_char	; if LF (10) the add CR (13)
	inc	word [.counter]	; line counter++
	cmp	word [.counter],24
	je	.l_endpage
.l_next:
	jmp	.l_read
.l_endpage:

	mov	al,[more]
	or	al,al
	jz	.l_read

	mov	dx,s_press_any_key
	call	print_string
	
	mov	ah,1	; getchar to al
	int	0x91
	call	crlf
	cmp	al,'q'
	je	.l_eof	; q - quit, go to close file and quit

	xor	bx,bx
	mov	[.counter],bx
	
	jmp	.l_read
.l_eof:
	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91

	jmp	l_loop
	
.l_err:
	mov	dx,s_err
	call	print_string
	jmp	l_loop

.counter:	dw	0

; load file to segment 5050 (area #2) ----------------------
l_pgm_5:	; load file to segment 5050 (area #2)

	mov	dword [stored32],0

	mov	dx,s_filename
	call	print_string

	mov	ax,buffer	
	call	getstr
	
	call	crlf
	mov	dx,buffer
	call	print_string
	call	crlf
	
	mov	ah,0x20	; open file
	mov	al,0	; O_RDONLY
	mov	bx,buffer	; filename
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err
	mov	dx,ax	; file descriptor
	call	ohw
	call	crlf

	mov	ax,0x5050
	mov	ES,ax	; ES:=0x5050

	xor	bp,bp
;;;;;;;;;;;;;;;;;;
.l_read:
	mov	ah,0x21	; read 1 byte from file
	mov	al,0	; file descriptor
	int	0x91
	
	cmp	ah,0
	jne	.l_eof

;	store byte to memory
	mov	[ES:bp],al
	inc	bp

	or	bp,bp	; if 0 (segment boundary overflow) then ES+=0x1000
	jnz	.l_next
	mov	ax,ES
	add	ax,0x1000
	mov	ES,ax
	
.l_next:

	inc	dword [stored32]
	jmp	.l_read
;;;;;;;;;;;;;;;;;;
.l_eof:
	push	CS
	pop	ES	; restore ES

	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91

	mov	eax,[stored32]
	call	ohd
	call	crlf

.l_err:
	jmp	l_loop
; end of load file -------------------------------------

; cat loaded file
l_pgm_6:
	mov	eax,[stored32]
	call	ohd
	call	crlf
	
	mov	ecx,[stored32]
	test	ecx,ecx
	jz	.l_0

	mov	ax,0x5050
	mov	ES,ax
	
	xor	bp,bp

.l_loop:
	push	ecx
	mov	al,[ES:bp]
	call	print_char
	inc	bp
	or	bp,bp	; if 0 (segment boundary overflow) then ES+=0x1000
	jnz	.l_next
	mov	ax,ES
	add	ax,0x1000
	mov	ES,ax
.l_next:
	pop	ecx
	dec	ecx
	or	ecx,ecx
	jnz	.l_loop

	push	CS
	pop	ES

	jmp	l_loop
	
.l_0:
	mov	dx,.s_0
	call	print_string
	jmp	l_loop
.s_0:	db	"No loaded file",13,10,'$'
; end of cat loaded file------------------------------

; run loaded file
l_pgm_0:
	mov	eax,[stored32]
	or	eax,eax
	jz	.l_no_file

	push	CS
	pop	bx

        mov	ax,0x5050	; segment
        mov	DS,ax
	mov	ES,ax
        cli
        mov	SS,ax
        mov	SP,0xFFF0
        sti

	; EXEC!
	jmp	0x5050:0x0000

.l_no_file: db "No loaded file",13,10,0

; end of run loaded file

; write file from segment 5050 (area #2) ----------------------
l_pgm_8:	;

	mov	eax,[stored32]
	or	eax,eax
	jz	.l_no_file

	mov	dx,s_filename
	call	print_string

	mov	ax,buffer	
	call	getstr
	
	call	crlf
	
	mov	ah,0x20	; open file
	mov	al,2	; O_CREAT
	mov	bx,buffer	; filename
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err_open
	mov	dx,ax	; file descriptor
	call	ohw
	call	crlf

	mov	ax,0x5050
	mov	ES,ax	; ES:=0x5050

	xor	bp,bp
	mov	eax,[stored32]
;;;;;;;;;;;;;;;;;;
.l_write:
	push	eax
	; write byte from memory
	mov	dl,[ES:bp]

	mov	ah,0x22	; write 1 byte to file
	mov	al,0	; file descriptor
	int	0x91
	
	cmp	ah,0
	jne	.l_err_write

	inc	bp
	or	bp,bp	; if 0 (segment boundary overflow) then ES+=0x1000
	jnz	.l_next
	mov	ax,ES
	add	ax,0x1000
	mov	ES,ax
.l_next:
	pop	eax
	dec	eax
	jnz	.l_write
;;;;;;;;;;;;;;;;;;

	push	CS
	pop	ES	; restore ES

	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91

	cmp	ah,0
	jne	.l_err_close
	jmp	l_loop

.l_err_open:
	mov	dx,.s_err_open
	call	print_string
	
	jmp	l_loop
.s_err_open:	db	"Can't open file for write",13,10,0

.l_err_write:
	mov	dx,.s_err_write
	call	print_string
	
	jmp	l_loop
.s_err_write:	db	"Can't write to file",13,10,0

.l_err_close:
	mov	dx,.s_err_close
	call	print_string
	
	jmp	l_loop
.s_err_close:	db	"Can't close writed file",13,10,0

.l_no_file:
	mov	dx,.s_err_no_file
	call	print_string
	
	jmp	l_loop
.s_err_no_file:	db	"No loaded file",13,10,0
; end of write file -------------------------------------

; write long file from segment 5050 (area #2) ----------------------
l_pgm_W:	;

	mov	dx,.s_vvedi_chislo
	call	print_string

	mov	ax,buffer
	call	getstr

	mov	bx,buffer
	call	htoeax
	mov	[length32],eax
	or	eax,eax
	jz	.l_no_file

	call	crlf
	mov	eax,[length32]
	call	ohd
	call	crlf

	mov	dx,s_filename
	call	print_string

	mov	ax,buffer	
	call	getstr
	
	call	crlf
	
	mov	ah,0x20	; open file
	mov	al,2	; O_CREAT
	mov	bx,buffer	; filename
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err_open
	mov	dx,ax	; file descriptor
	call	ohw
	call	crlf

	mov	ax,0x5050
	mov	ES,ax	; ES:=0x5050

	xor	ebp,ebp
;;;;;;;;;;;;;;;;;;
.l_write:
	; write byte from memory
	mov	dl,[ES:bp]

	mov	ah,0x22	; write 1 byte to file
	mov	al,0	; file descriptor
	int	0x91
	
	cmp	ah,0
	jne	.l_err_write

	inc	ebp

	cmp	ebp,[length32]
	je	.l_break
	
	jmp	.l_write
;;;;;;;;;;;;;;;;;;
.l_break:
	push	CS
	pop	ES	; restore ES

	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91

	cmp	ah,0
	jne	.l_err_close
	jmp	l_loop

.l_err_open:
	mov	dx,.s_err_open
	call	print_string
	
	jmp	l_loop
.s_err_open:	db	"Can't open file for write",13,10,0

.l_err_write:
	mov	dx,.s_err_write
	call	print_string
	
	jmp	l_loop
.s_err_write:	db	"Can't write to file",13,10,0

.l_err_close:
	mov	dx,.s_err_close
	call	print_string
	
	jmp	l_loop
.s_err_close:	db	"Can't close writed file",13,10,0

.l_no_file:
	mov	dx,.s_err_no_file
	call	print_string
	
	jmp	l_loop
.s_err_no_file:	db	"Zero length!",13,10,0
.s_vvedi_chislo:	db	"Enter file size > ",13,10,0

length32:	dd	0
; end of write file -------------------------------------

;--------------------------------------------------------

print_help:
	push	ax
	push	dx
	
	mov	dx,s_help
	call	print_string
	
	pop	dx
	pop	ax
	ret

l_clisti:
	cli
	sti
	jmp	l_loop
	
l_cli:
	cli
	jmp	l_loop
	
l_sti:
	sti
	jmp	l_loop
	
l_exit:
    int 0x90 ; exit

; view exe header -------------------------------------------------------------------------------------------------------------

l_pgm_e:	; load exe file to segment 5050 (area #2)

	mov	dword [stored32],0

	mov	dx,s_filename
	call	print_string

	mov	ax,buffer	
	call	getstr
	
	call	crlf
	mov	dx,buffer
	call	print_string
	call	crlf
	
	mov	ah,0x20	; open file
	mov	al,0	; O_RDONLY
	mov	bx,buffer	; filename
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err
	mov	dx,ax	; file descriptor
	call	ohw
	call	crlf

	mov	ax,0x5050
	mov	ES,ax	; ES:=0x5050

	xor	bp,bp
;;;;;;;;;;;;;;;;;;
.l_read:
	mov	ah,0x21	; read 1 byte from file
	mov	al,0	; file descriptor
	int	0x91
	
	cmp	ah,0
	jne	.l_view_exe

;	store byte to memory
	mov	[ES:bp],al
	inc	bp

	or	bp,bp	; if 0 (segment boundary overflow) then ES+=0x1000
	jnz	.l_next
	mov	ax,ES
	add	ax,0x1000
	mov	ES,ax
	
.l_next:

	mov	eax, [stored32]
	inc	eax
	mov	[stored32],eax

	cmp	eax,0x30
	je	.l_view_exe

	jmp	.l_read
;;;;;;;;;;;;;;;;;;
.l_eof:
	push	CS
	pop	ES	; restore ES

	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91

	mov	eax,[stored32]
	call	ohd
	call	crlf

.l_err:
	mov	dx,.s_open_err1
	call	print_string
	jmp	l_loop
.s_open_err1:	db	"Can't open file",13,10,0

.l_view_exe:

	; close file
	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err4

	mov	ax,0x5050
	mov	ES,ax

	mov	ax,ES:[0]
	cmp	ax,0x5a4d	; compare with MZ signature
	je	.l_is_exe
	cmp	ax,0x4d5a	; ZM signature
	je	.l_is_exe
	; no exe signature
	mov	dx,.s_no_exe
	call	print_string
	jmp	.l_1

.l_is_exe:
	mov	dx,.s_is_exe
	call	print_string
.l_1:

	mov	ax,ES:[8]	; header size, par
	shl	ax,4		; header size, bytes
	mov	[ExeHeaderSize],ax

	mov	ax,ES:[0x18]	; relocation table begin
	mov	[TabBegin],ax

	mov	ax,ES:[6]	; relocation table size
	mov	[TabSize],ax

	mov	dx,.s_length_mod512
	call	print_string
	mov	ax,ES:[2]
	call	ohw
	call	crlf

	mov	dx,.s_exe_4
	call	print_string
	mov	ax,ES:[4]
	call	ohw
	call	crlf

	mov	dx,.s_exe_6
	call	print_string
	mov	ax,ES:[6]
	call	ohw
	call	crlf

	mov	dx,.s_exe_8
	call	print_string
	mov	ax,ES:[8]
	call	ohw
	call	crlf

	mov	dx,.s_exe_0xa
	call	print_string
	mov	ax,ES:[0xa]
	call	ohw
	call	crlf

	mov	dx,.s_exe_0xc
	call	print_string
	mov	ax,ES:[0xc]
	call	ohw
	call	crlf

	mov	dx,.s_exe_0xe
	call	print_string
	mov	ax,ES:[0xe]
	mov	[ExeSS],ax
	call	ohw
	call	crlf

	mov	ax,[ExeSS]
	add	ax,0x5050
	mov	[ExeSS],ax

	mov	dx,.s_exe_0x10
	call	print_string
	mov	ax,ES:[0x10]
	mov	[ExeSP],ax
	call	ohw
	call	crlf

	mov	dx,.s_exe_0x12
	call	print_string
	mov	ax,ES:[0x12]
	call	ohw
	call	crlf

	mov	dx,.s_exe_0x14
	call	print_string
	mov	ax,ES:[0x14]
	mov	[ExeIP],ax
	call	ohw
	call	crlf

	mov	dx,.s_exe_0x16
	call	print_string
	mov	ax,ES:[0x16]
	mov	[ExeCS],ax
	call	ohw
	call	crlf

	mov	ax,[ExeCS]
	add	ax,0x5050
	mov	[ExeCS],ax

	mov	dx,.s_exe_0x18
	call	print_string
	mov	ax,ES:[0x18]
	call	ohw
	call	crlf

	mov	dx,.s_exe_0x1a
	call	print_string
	mov	ax,ES:[0x1a]
	call	ohw
	call	crlf

	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91

; reopen file

	mov	dx,s_quote
	call	print_string

	mov	dx,buffer
	call	print_string

	mov	dx,s_quote
	call	print_string

	call	crlf

	push	DS
	pop	ES	; restor ES на всякий случай

	mov	ah,0x20	; open file
	mov	al,0	; O_RDONLY
	mov	bx,buffer	; filename
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err2

; skip header

	mov	cx,[ExeHeaderSize]
.l_skip:
	push	cx
	mov	ah,0x21	; read 1 byte from file
	mov	al,0	; file descriptor
	int	0x91

	cmp	ah,0
	jne	.l_err3

	pop	cx	
	loop	.l_skip

; load to RAM

	mov	bp,0x100; load exe file body to 5050:0100
	mov	ax,0x5050
	mov	ES,ax
.l_next2:
	mov	ah,0x21	; read 1 byte from file
	mov	al,0	; file descriptor
	int	0x91
	
	cmp	ah,0
	jne	.l_2

;	store byte to memory
	mov	[ES:bp],al
	inc	bp

	or	bp,bp	; if 0 (segment boundary overflow) then ES+=0x1000
	jnz	.l_next2
	mov	ax,ES
	add	ax,0x1000
	mov	ES,ax
	jmp	.l_next2
	
.l_2:
; close file
	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91

; reopen file
	push	DS
	pop	ES	; restor ES на всякий случай

	mov	ah,0x20	; open file
	mov	al,0	; O_RDONLY
	mov	bx,buffer	; filename
	int	0x91
	cmp	ax,0xFFFF
	je	.l_err21

; skip to rel. table

	mov	cx,[TabBegin]
	or	cx,cx
	jz	.l_no_tab
.l_skip2:
	push	cx
	mov	ah,0x21	; read 1 byte from file
	mov	al,0	; file descriptor
	int	0x91

	cmp	ah,0
	jne	.l_err31

	pop	cx	
	loop	.l_skip2


; read rel. table
	mov	cx,[TabSize]
	or	cx,cx
	jz	.l_no_tab
.l_tab:
	push	cx

	mov	bp,0	; временно как буфер используем первые 4 байта PSP 
	mov	ax,0x5050
	mov	ES,ax
	mov	cx,4

.l_read_record:
	push	cx
	; read 4 bytes
	mov	ah,0x21	; read 1 byte from file
	mov	al,0	; file descriptor
	int	0x91

	cmp	ah,0
	jne	.l_err32

	inc	bp
	pop	cx
	loop	.l_read_record

	; настройка адреса
	mov	bp,[ES:0] ; offset
	mov	ax,[ES:2] ; segment
	add	ax,0x5050
	push	ES
	mov	ES,ax
	mov	ax,[ES:bp] ; то слово, которое надо скорректировать
	add	ax,0x5050
	mov	[ES:bp],ax
	pop	ES

	pop	cx
	loop	.l_tab

.l_no_tab:
; close file
	mov	ah,0x23	; close file
	mov	al,0	; file descriptor
	int	0x91

	mov	dx,.s_exec
	call	print_string

; make PSP
	mov	ax,0x5050
	mov	ES,ax

	mov	ax,0x20CD
	mov	[ES:0],ax	; int 20 code in PSP addr 0
	mov	ax,0x9FF0
	mov	[ES:2],ax	; сегмент конца памяти

	mov	ax,Environ	
	mov	[ES:0x2c],ax

; EXEC!
	cli
	mov	ax,[ExeSS]
	mov	SS,ax	
	mov	ax,[ExeSP]
	mov	SP,ax
	sti

	mov	ax,[ExeCS]
	push	ax
	mov	ax,[ExeIP]
	push	ax
	mov	ax,0x05050
	mov	ES,ax
	mov	DS,ax
	retf	

.l_err2:
	mov	dx,.s_reopen_err
	call	print_string
	jmp	l_loop

.l_err21:
	mov	dx,.s_reopen_err2
	call	print_string
	jmp	l_loop

.l_err3:
	mov	dx,.s_err3
	call	print_string
	jmp	l_loop

.l_err31:
	mov	dx,.s_err31
	call	print_string
	jmp	l_loop

.l_err32:
	mov	dx,.s_err32
	call	print_string
	jmp	l_loop

.l_err4:
	mov	dx,.s_err4
	call	print_string
	jmp	l_loop

.l_exit:
	push	CS
	pop	ES

	jmp	l_loop

.s_exec:	db	"Exec!",13,10,0
.s_err4:	db	"Close error",13,10,0
.s_err3:	db	"Skip header error",13,10,0
.s_err31:	db	"Skip to relo tab error",13,10,0
.s_err32:	db	"Read relo tab error",13,10,0
.s_reopen_err:	db	"Reopen error",13,10,0
.s_reopen_err2:	db	"Reopen error #2",13,10,0
.s_no_exe:	db	"This is NO exe file",13,10,0
.s_is_exe:	db	"This is EXE file",13,10,0
.s_length_mod512:	db	"Length mod 512 or 4 : $"
.s_exe_4:	db	"File len, blocks : $"
.s_exe_6:	db	"Tab size : $"
.s_exe_8:	db	"Header size, par : $"
.s_exe_0xa:	db	"Min mem, par : $"
.s_exe_0xc:	db	"Max mem, par : $"
.s_exe_0xe:	db	"SS : $"
.s_exe_0x10:	db	"SP : $"
.s_exe_0x12:	db	"CRC : $"
.s_exe_0x14:	db	"IP : $"
.s_exe_0x16:	db	"CS : $"
.s_exe_0x18:	db	"Tab begin : $"
.s_exe_0x1a:	db	"Overlay no : $"

ExeHeaderSize:	dw	0
TabSize:	dw	0
TabBegin:	dw	0
Environ:	dw	0
ExeSS:		dw	0
ExeSP:		dw	0
ExeCS:		dw	0
ExeIP:		dw	0
; end of view exe header -------------------------------------

%include "libnasm.asm"

s_ver:	db	"Proolix asm program, compiled by nasm ",  __?DATE?__, " ", __?TIME?__
	db	13,10,13,10,'$'

s_prompt:	db	"u1 cmd > $"
s_str2:	db	13,10,"You enter char $"
s_filename:	db	"Filename > $"
s_press_any_key:	db	"press any key (q - quit)$"
s_memmap:	db	"Memory map for Proolix-l (real mode)",13,10,"\
0 - 1FF  Vectors",13,10,"\
200 - 3FF free area (stack of boot sector)",13,10,"\
400 - 4FF ROM BIOS data: 475: count of HDD",13,10,"\
600 - 614 - list of lists (= 0000:0600 = 0060:0000)",13,10,"\
07C00 =0:7C00 (=0070:7500) Boot sector (stage1)",13,10,"\
07E00                      Boot sector end",13,10,"\
30000 or 30500 stage2 ('ct' file or kernel, 3000 - fdd, 3050 - hdd)",13,10,"\
3FFFF or 404FF - end of kernel",13,10,"\
40500 - memory area #1",13,10,"\
50500 - memory area #2",13,10,"\
MemTop (f.e. 9FFFF)",13,10,"\
B8000 CGA, EGA, VGA 1st videopage (mode=3, symbol mode)",13,10,"\
C8000 Additional ROM modules (2K blocks)",13,10,"\
E0000 Bg  AT ROM BIOS",13,10,"\
EFFFF End AT ROM BIOS",13,10,"\
F6000 ROM Basic",13,10,"\
FE000 ROM BIOS, POST",13,10,"\
FFFF0 JMP - COLD REBOOT",13,10,"\
FFFF5 BIOS version/date (ASCII)",13,10,"\
FFFFE PC/XT/AT identification byte\
",13,10,'$'

s_proolix_help: db "Proolix-l command:",13,10,"\
help, ? - this help",13,10,"\
ver - version",13,10,"\
ascii - write ascii table",13,10,"\
off - screensaver (clear screen and wait of press any key",13,10,"\
scr - screensaver #2",13,10,"\
sysinfo - print system parameters",13,10,"\
memd0 - memory dump for extended processor mode",13,10,"\
memd - memory dump for real mode",13,10,"\
vec - print interrupt vectors",13,10,"\
basic - call ROM BASIC (if exist)",13,10,"\
diskd0 - disk dump #1 (sector/head/track)",13,10,"\
diskd - disk dump #2 (absolute sector)",13,10,"\
sk - run prool skript",13,10,"\
time - print time",13,10,"\
install - install Proolix-l to other disk format - format filesystem",13,10,"\
super - view superblock ls - ls creat - create file rm - remove file",13,10,"\
tofile - string to file tofile2 - debuggin command dd - dd",13,10,"\
cat - cat file hcat - hex cat file cp - copy files",13,10,"\
reboot - reboot cold - cold reboot",13,10,"\
hdd0 - boot from HDD0 hdd1 - boot from HDD1 fdd - boot from FDD",13,10,"\
settimezone - set tz videomod - set video mode run - run rundos - run DOS",13,10,"\
dosarg - enter DOS arguments; FCB - list FCBs",13,10,'$'

s_help:	db	"Menu",13,10,13,10
	db	"q - quit",13,10
	db	"? - u1 help",13,10
	db	"m - memmap",13,10
	db	"h - proolix help",13,10
	db	"c - cli/sti "
	db	"1 - cli "
	db	"2 - sti",13,10
	db	"3 - cat",13,10
	db	"4 - create file",13,10
	db	"5 - load file",13,10
	db	"6 - cat loaded file",13,10
	db	"7 - hexcat loaded file (n/a)",13,10
	db	"8 - write loaded file",13,10
	db	"9 - binary diff (n/a)",13,10
	db	"0 - run loaded file",13,10
	db	"M - more on/off",13,10
	db	"W - write long file",13,10
	db	"e - view exe header",13,10
	db	"t - test",13,10
	db	13,10,'$'

s_err:	db	"Error",13,10,0
s_quote:	db	'"',0
buffer:
; end of file
