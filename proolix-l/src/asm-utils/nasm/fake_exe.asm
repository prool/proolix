; fake EXE
; assembler program for OS Proolix
; for compiling need nasm
;
; interrupt 0x91 - service interrupt in Proolix (similar to MSDOS's int 0x21)
; int 0x90 - quit in Proolix (similar to int 0x20 in MSDOS)
; int 0x21 - also working in Proolix kernel inbound simplest DOS-emulator

; /prool, 2023, Kharkiv, Ukraine, war
; www.prool.kharkov.org

    org 0 ; begin addr for binaryes

;	exe header
	db	"MZ"	; magick

	dw	0x4	; Длина образа задачи по модулю 512 (то есть число полезных байт в последнем блоке). Компоновщики версий до 1.10 помещали в это поле 04; если оно имеет такое значение, его рекомендуется игнорировать);

	dw	0	; Длина файла в блоках;
	dw	0	; Число элементов таблицы настройки адресов;
	dw	2	; Длина заголовка в 16-байтных параграфах. Используется для выяснения начала тела загрузочного модуля;
	dw	0	; Минимальный объём памяти, которую нужно выделить после конца образа задачи (в 16-байтных параграфах);
	dw	0	; Максимальный объём памяти, которую нужно выделить после конца образа задачи (в 16-байтных параграфах);
	dw	0	; Сегментный адрес начала стекового сегмента относительно начала образа задачи;
	dw	0xFFFE	; Значение SP при входе в задачу;
	dw	0	; Контрольная сумма — ноль минус результат сложения без переноса всех слов файла;
	dw	0x100	; Значение IP (счетчика команд) при входе в задачу;
	dw	0	; Сегментный адрес начала кодового сегмента относительно начала образа задачи;
	dw	0	; Адрес первого элемента таблицы настройки адресов относительно начала файла;
	dw	0	; Номер сегмента перекрытий (0 для корневого сегмента программы).

;	таблица настройки адресов (пока нет!)

; ручное выравнивание до границы параграфа
	dw	0
	dw	0
exe_begin:
;	push CS
;	pop	DS

;	push CS
;	pop ES

;	push	cs
;	pop	bx

;	mov	SS,bx
;	mov	SP,0xFFFC

	mov al,'!'
	call	print_char
	
	mov	ax,0x1234
	call	ohw
	
	push	CS
	pop	ax
	call	ohw
	
	push	DS
	pop	ax
	call	ohw
	
	push	ES
	pop	ax
	call	ohw
	
	push	SS
	pop	ax
	call	ohw
	
	call	next
next:
	pop	ax
	call	ohw

;l_stop1:	jmp	l_stop1

	mov	dx,message+0x100-0x20	; correction for exe: 0x100 - это адрес загрузки exe, а 20 - адрес времени компиляции
					; (см. метку exe_begin)
	call	print_string

	int	0x90	; QUIT
		
l_loop:	jmp	l_loop

message:	db	"Program fake exe ",13,10,0

%include "libnasm.asm"
