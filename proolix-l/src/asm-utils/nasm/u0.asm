; assembler program for OS Proolix
; for compiling need nasm
;
; interrupt 0x91 - service interrupt in Proolix (similar to MSDOS's int 0x21)
; int 0x90 - quit in Proolix (similar to int 0x20 in MSDOS)
; int 0x21 - also working in Proolix kernel inbound simplest DOS-emulator

; /prool, 2023, Kharkiv, Ukraine, war
; www.prool.kharkov.org

    org 0x0 ; begin addr for proolix binaryes

	jmp	l_continue
signature:	dw	0xAAAA
arguments_offset:	dw	0
arguments_seg:		dw	0
	dw	0	; protect minibuffer

l_continue:

	push CS
	pop	DS

	push CS
	pop ES

	push	cs
	pop	bx

	;cli
	mov	SS,bx
	mov	SP,0xFFFC
	;sti

	mov al,'!'
	call	print_char
    
	mov	dx,message
	call	print_string

	int	0x90	; QUIT
		
l_loop:	jmp	l_loop

message:	db	"Program u0$"

%include "libnasm.asm"
